# Rustone

Minimal hearthstone implementation in Rust. Designed to be fast, thread safe, and easy to expand upon. *Requires nightly rust*.

## Getting Started

### Installing Rust
If you don't have rustup, install it from https://rustup.rs/.

If you don't have rust nightly, you can install it and make it default with the following:
```
rustup install nightly
rustup default nightly
```

### Cloning
```
git clone git@gitlab.com:onnoowl/rustone.git
cd rustone
```

### Building
```
cargo build
```

### Running
```
cargo run -p rustone_cmd
```
