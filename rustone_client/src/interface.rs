pub use rustone_lib::message::{
    ClientGameState, Deck, GameError, Input, InputNeeded, KeepOrDiscard, Outcome, Player,
};

pub trait ClientInterface {
    fn new(player: &Player) -> Self;
    fn get_deck_choice(&mut self) -> Deck;
    fn get_init_card_choices(&mut self, card_options: &[u32]) -> Vec<KeepOrDiscard>;
    fn get_input(&mut self, game_state: &ClientGameState, needed: &InputNeeded) -> Input;
    fn notify_error(&mut self, err: &GameError) {
        eprintln!("{}, err", err)
    }
    fn notify_new_game_state(&mut self, _game_state: &ClientGameState) {}
    fn notify_outcome(&mut self, outcome: &Outcome) {
        match outcome {
            Outcome::Won(player) => println!("Player {:?} has won", player),
            Outcome::Draw => println!("The game has ended in a draw"),
        }
    }
}
