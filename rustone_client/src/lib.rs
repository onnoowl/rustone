mod interface;
mod msg_stream;
pub use crate::interface::*;
use crate::msg_stream::*;

use std::net::TcpStream;

use rustone_lib::message::*;

pub struct Client<I: ClientInterface> {
    interface: I,
    stream: MsgStream,
}

impl<I: ClientInterface> Default for Client<I> {
    fn default() -> Self {
        Self::new("127.0.0.1:25565").expect("Failed to connect to localhost")
    }
}

impl<I: ClientInterface> Client<I> {
    pub fn new(addr: &str) -> Result<Self, std::io::Error> {
        loop {
            let mut server = MsgStream::new(TcpStream::connect(addr).map_err(|err| {
                eprintln!("Failed to connect to addr {} with error: {}", addr, err);
                err
            })?);
            let player = match server.read() {
                Some(ServerMsg::NoReply(NoReplyMsg::Init(player))) => player,
                Some(other) => {
                    eprintln!("Error, unexpected initial server message {:?}", other);
                    continue;
                }
                None => continue,
            };
            return Ok(Client::<I> {
                interface: I::new(&player),
                stream: server,
            });
        }
    }

    pub fn start_loop(&mut self) {
        loop {
            match self.stream.read() {
                Some(ServerMsg::ReplyNeeded(reply_needed)) => {
                    let reply = match reply_needed {
                        ReplyNeededMsg::GetDeckChoice => {
                            ClientMsg::GiveDeckChoice(self.interface.get_deck_choice())
                        }
                        ReplyNeededMsg::GetInitCardChoices(options) => {
                            ClientMsg::GiveInitCardChoices(
                                self.interface.get_init_card_choices(&options),
                            )
                        }
                        ReplyNeededMsg::GetInput(game_state, input_needed) => ClientMsg::GiveInput(
                            self.interface.get_input(&game_state, &input_needed),
                        ),
                    };
                    if self.stream.write(&reply).is_none() {
                        break;
                    }
                }
                Some(ServerMsg::NoReply(no_reply)) => match no_reply {
                    NoReplyMsg::NotifyError(game_error) => self.interface.notify_error(&game_error),
                    NoReplyMsg::NotifyNewGameState(game_state) => {
                        self.interface.notify_new_game_state(&game_state)
                    }
                    NoReplyMsg::NotifyOutcome(outcome) => self.interface.notify_outcome(&outcome),
                    other => {
                        eprintln!("Error, unexpected server msg {:?}", other);
                        break;
                    }
                },
                None => break,
            }
        }
    }
}
