use rustone_lib::card_type::CardType;
use rustone_lib::cards::core::neutral::*;
use rustone_lib::cards::expert::neutral::*;
use rustone_lib::state::Deck;

pub fn basic_deck() -> Deck {
    let mut deck = Deck::new("sample_deck".to_string());

    deck.cards.extend(vec![
        BoulderfistOgre::id(),
        BoulderfistOgre::id(),
        SenjinShieldmasta::id(),
        SenjinShieldmasta::id(),
        ArgentSquire::id(),
        ArgentSquire::id(),
        OasisSnapjaw::id(),
        OasisSnapjaw::id(),
        StranglethornTiger::id(),
        StranglethornTiger::id(),
        WindfuryHarpy::id(),
        WindfuryHarpy::id(),
        Wolfrider::id(),
        Wolfrider::id(),
        ChillwindYeti::id(),
        ChillwindYeti::id(),
        WarGolem::id(),
        WarGolem::id(),
        Gnoll::id(),
        Gnoll::id(),
    ]);

    deck
}

pub fn sample_deck() -> Deck {
    let mut deck = Deck::new("sample_deck".to_string());

    deck.cards.extend(vec![
        BoulderfistOgre::id(),
        BoulderfistOgre::id(),
        StormwindChampion::id(),
        StormwindChampion::id(),
        SenjinShieldmasta::id(),
        SenjinShieldmasta::id(),
        HarvestGolem::id(),
        HarvestGolem::id(),
        ArgentSquire::id(),
        ArgentSquire::id(),
        RaidLeader::id(),
        RaidLeader::id(),
        FrostwolfWarlord::id(),
        FrostwolfWarlord::id(),
    ]);

    deck
}
