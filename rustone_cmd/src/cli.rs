use std::io;
use std::io::Write;

pub struct CLI<T> {
    commands: Vec<Command<T>>,
    input_buffer: String,
}

pub struct Command<T> {
    min_num_args: usize,
    max_num_args: usize,
    closure: Box<dyn Fn(&mut T, Vec<&str>)>,
    command: String,
    desc: String,
}

impl<T> CLI<T> {
    pub fn new() -> Self {
        CLI {
            commands: Vec::new(),
            input_buffer: String::new(),
        }
    }

    pub fn push(&mut self, command: Command<T>) -> &mut Self {
        self.commands.push(command);
        self
    }

    pub fn cmd_no_args<F: Fn(&mut T) + 'static>(
        &mut self,
        command: String,
        desc: String,
        fun: F,
    ) -> &mut Self {
        self.push(Command {
            min_num_args: 0,
            max_num_args: 0,
            closure: Box::new(move |state: &mut T, _args: Vec<&str>| (fun)(state)),
            command,
            desc,
        });
        self
    }

    pub fn cmd_one_arg<F: Fn(&mut T, &str) + 'static>(
        &mut self,
        command: String,
        desc: String,
        fun: F,
    ) -> &mut Self {
        self.push(Command {
            min_num_args: 1,
            max_num_args: 1,
            closure: Box::new(move |state: &mut T, args: Vec<&str>| (fun)(state, args[0])),
            command,
            desc,
        });
        self
    }

    pub fn cmd_opt_arg<F: Fn(&mut T, Option<&str>) + 'static>(
        &mut self,
        command: String,
        desc: String,
        fun: F,
    ) -> &mut Self {
        self.push(Command {
            min_num_args: 0,
            max_num_args: 1,
            closure: Box::new(move |state: &mut T, args: Vec<&str>| {
                (fun)(state, args.get(0).cloned())
            }),
            command,
            desc,
        });
        self
    }

    pub fn cmd_with_args<F: Fn(&mut T, Vec<&str>) + 'static>(
        &mut self,
        command: String,
        desc: String,
        min_num_args: usize,
        max_num_args: usize,
        fun: F,
    ) -> &mut Self {
        self.push(Command {
            min_num_args,
            max_num_args,
            closure: Box::new(fun),
            command,
            desc,
        });
        self
    }

    pub fn cmd_any_args<F: Fn(&mut T, Vec<&str>) + 'static>(
        &mut self,
        command: String,
        desc: String,
        fun: F,
    ) -> &mut Self {
        self.push(Command {
            min_num_args: 0,
            max_num_args: std::usize::MAX,
            closure: Box::new(fun),
            command,
            desc,
        });
        self
    }

    pub fn default_help(&mut self) -> &mut Self {
        let help_commands = self
            .commands
            .iter()
            .map(|cmd| format!("    {} : {}", cmd.command, cmd.desc))
            .collect::<Vec<_>>()
            .join("\n");
        let help_string = format!(
            "Enter 'h' or 'help' to see this help message.\n{}",
            help_commands
        );
        self.cmd_no_args(
            "help".to_string(),
            "Print this help message".to_string(),
            move |_state| {
                println!("{}", help_string);
            },
        );
        self
    }

    /** Loops until it gets the next valid command, and executes it. Then returns. */
    pub fn next(&mut self, state: &mut T) -> Result<(), io::Error> {
        loop {
            self.input_buffer.clear();
            print!("> ");
            io::stdout().flush()?;
            io::stdin().read_line(&mut self.input_buffer)?;
            let mut split_iter = self.input_buffer.split_whitespace();
            if let Some(cmd) = split_iter.next() {
                let matching: Vec<_> = self
                    .commands
                    .iter()
                    .filter(|command| command.command.starts_with(cmd))
                    .collect();

                match &matching[..] {
                    [] => println!(
                        "Error, no command begins with '{}'. Enter 'h' for help.",
                        cmd
                    ),
                    [cmd] => {
                        let arguments: Vec<&str> = split_iter.collect();

                        let too_many = arguments.len() > cmd.max_num_args;
                        let too_few = arguments.len() < cmd.min_num_args;
                        if too_many || too_few {
                            let word = if too_few { "few" } else { "many" };
                            let number_of_args = if cmd.max_num_args == cmd.min_num_args {
                                cmd.max_num_args.to_string()
                            } else {
                                format!("{} to {}", cmd.min_num_args, cmd.max_num_args)
                            };
                            println!(
                                "Error, too {} arguments. Expected {} args, got {}",
                                word,
                                number_of_args,
                                arguments.len()
                            )
                        } else {
                            (cmd.closure)(state, arguments);
                            return Ok(());
                        }
                    }
                    _ => println!(
                        "Error, multiple commands begin with '{}': {}.",
                        cmd,
                        matching
                            .iter()
                            .map(|m| m.command.as_str())
                            .collect::<Vec<_>>()
                            .join(", ")
                    ),
                }
            }
        }
    }

    /** Will loop calling self.next() indefinitely. */
    pub fn start_loop(&mut self, state: &mut T) -> Result<(), io::Error> {
        loop {
            self.next(state)?;
        }
    }
}

impl<T> Command<T> {}
