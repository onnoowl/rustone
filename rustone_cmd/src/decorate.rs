use colored::*;
use regex::Regex;

use rustone_lib::card_type::Rarity;

pub fn with_rarity(s: &str, r: &Rarity) -> ColoredString {
    match r {
        Rarity::NONE => s.clear(),
        Rarity::COMMON => s.clear(),
        Rarity::FREE => s.clear(),
        Rarity::RARE => s.blue(),
        Rarity::EPIC => s.purple(),
        Rarity::LEGENDARY => s.yellow(),
    }
}

pub fn html(opt: Option<&str>) -> String {
    if let Some(s) = opt {
        let re = Regex::new(r"\s+").unwrap();
        let s = re.replace_all(s, " ").to_string();
        let s = tag_to_decorate(&s, 'b', |x| x.bold());
        tag_to_decorate(&s, 'i', |x| x.italic())
    } else {
        String::from("")
    }
}

fn tag_to_decorate<T: Fn(&str) -> ColoredString>(
    string: &str,
    tag_char: char,
    dec_fun: T,
) -> String {
    let mut s = String::from(string);
    let re = Regex::new(&format!(r"(.*)<{0}>(.*?)</{0}>(.*)", tag_char)).unwrap();
    loop {
        s = if let Some(caps) = re.captures(&s) {
            format!(
                "{}{}{}",
                caps.get(1).map(|s| s.as_str()).unwrap_or(""),
                dec_fun(caps.get(2).map(|s| s.as_str()).unwrap_or("")),
                caps.get(3).map(|s| s.as_str()).unwrap_or("")
            )
        } else {
            break;
        };
    }
    s
}
