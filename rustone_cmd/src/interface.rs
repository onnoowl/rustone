pub use rustone_client::*;

pub struct CmdInterface {
    player: Player,
}

impl ClientInterface for CmdInterface {
    fn new(player: &Player) -> Self {
        CmdInterface {
            player: player.clone(),
        }
    }

    fn get_deck_choice(&mut self) -> Deck {
        unimplemented!()
    }

    fn get_init_card_choices(&mut self, _card_options: &[u32]) -> Vec<KeepOrDiscard> {
        unimplemented!()
    }

    fn get_input(&mut self, _game_state: &ClientGameState, _needed: &InputNeeded) -> Input {
        unimplemented!()
    }

    fn notify_new_game_state(&mut self, _game_state: &ClientGameState) {
        unimplemented!()
    }

    fn notify_outcome(&mut self, outcome: &Outcome) {
        match outcome {
            Outcome::Won(player) if player == &self.player => println!("You have won!"),
            Outcome::Won(_) => println!("You have lost!"),
            Outcome::Draw => println!("The game has ended in a draw"),
        }
    }
}
