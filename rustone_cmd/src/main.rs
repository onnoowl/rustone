mod builtin_decks;
mod cli;
mod decorate;
mod interface;

use colored::*;

use rustone_client::Client;
use rustone_lib::{card_type::CardType, cards, registry};

use crate::cli::CLI;
use crate::interface::CmdInterface;

fn main() -> Result<(), std::io::Error> {
    println!(
        "Card Example: {:?}",
        cards::expert::neutral::Deathwing::get()
    );

    let (implemented, unimplemented): (Vec<_>, Vec<_>) =
        registry::iter().partition(|card| card.mechs().unimplemented.inner.is_empty());
    let (needs_handler, simple): (Vec<_>, Vec<_>) = implemented
        .into_iter()
        .partition(|card| card.mechs().needs_handler);

    println!(
        "\n\n== ❌ Cards with unsupported features ({}) ==\n",
        unimplemented.len()
    );
    unimplemented.into_iter().for_each(simple_print);

    println!(
        "\n\n== ❎ Cards that need custom handlers ({}) ==\n",
        needs_handler.len()
    );
    needs_handler.into_iter().for_each(simple_print);

    println!(
        "\n\n== ✅ Cards that are ready to go ({}) ==\n",
        simple.len()
    );
    simple.into_iter().for_each(simple_print);

    CLI::new()
        .cmd_no_args(
            "foo".to_string(),
            "it does fooey things".to_string(),
            |_state| {
                println!("Bar");
            },
        )
        .cmd_no_args(
            "abba".to_string(),
            "it's a good band".to_string(),
            |_state| {
                println!("Take a chance on me");
            },
        )
        .cmd_no_args(
            "alloc".to_string(),
            "it allocs things".to_string(),
            |_state| {
                println!("Allocation made");
            },
        )
        .default_help()
        .start_loop(&mut ())?;

    let mut client = Client::<CmdInterface>::new("127.0.0.1:25565")?;
    client.start_loop();
    Ok(())
}

fn simple_print(card: &'static dyn CardType) {
    println!(
        "{:?} {:?} {:?} {} {}",
        card.class(),
        card.set(),
        card.card_use_type(),
        decorate::with_rarity(card.name(), &card.rarity()),
        decorate::html(card.text()).green()
    );
}

#[cfg(test)]
mod tests {
    use rustone_lib::registry;

    #[test]
    fn registry_works_cross_crate() {
        assert_eq!(179_u32, registry::get(179_u32).get_id());
    }
}
