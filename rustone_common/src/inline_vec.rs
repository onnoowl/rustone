use proc_macro2::TokenStream;
use quote::*;

#[derive(Clone, PartialEq, Debug)]
pub struct InlineVec<T> {
    pub inner: Vec<T>,
}

impl<T> InlineVec<T> {
    pub fn new(v: Vec<T>) -> Self {
        InlineVec { inner: v }
    }
}

impl<T: ToTokens> ToTokens for InlineVec<T> {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let inner = &self.inner;
        let code = quote! {
            InlineVec {
                inner: vec!(#(#inner),*)
            }
        };
        tokens.extend(code);
    }
}

impl<T> Default for InlineVec<T> {
    fn default() -> Self {
        InlineVec {
            inner: Default::default(),
        }
    }
}
