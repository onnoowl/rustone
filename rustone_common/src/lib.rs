#![allow(non_camel_case_types)]

pub mod inline_vec;
pub use crate::inline_vec::InlineVec;

#[derive(
    Default,
    Clone,
    PartialEq,
    Debug,
    totokens_derive::ToTokens,
    serde_derive::Serialize,
    serde_derive::Deserialize,
)]
pub struct Properties {
    // Incomplete.
    pub charge: bool,
    pub divine_shield: bool,
    pub taunt: bool,
    pub stealth: bool,
    pub windfury: bool,
}

#[derive(Default, Clone, PartialEq, Debug, totokens_derive::ToTokens)]
pub struct Mechanics {
    // Incomplete.
    pub props: Properties,
    pub aura: bool,
    pub adjacent_buff: bool,
    pub battlecry: bool,
    pub deathrattle: bool,
    pub enrage: bool,
    pub needs_handler: bool,
    pub unimplemented: InlineVec<Mechanic>,
}

impl<'a> From<&'a Vec<Mechanic>> for Mechanics {
    fn from(mechanics: &'a Vec<Mechanic>) -> Self {
        let mut m: Mechanics = Default::default();
        for mechanic in mechanics {
            match mechanic {
                Mechanic::CHARGE => {
                    m.props.charge = true;
                }
                Mechanic::DIVINE_SHIELD => {
                    m.props.divine_shield = true;
                }
                Mechanic::STEALTH => {
                    m.props.stealth = true;
                }
                Mechanic::TAUNT => {
                    m.props.taunt = true;
                }
                Mechanic::WINDFURY => {
                    m.props.windfury = true;
                }
                Mechanic::AURA => {
                    m.aura = true;
                    m.needs_handler = true;
                }
                Mechanic::ADJACENT_BUFF => {
                    m.adjacent_buff = true;
                    m.needs_handler = true;
                }
                Mechanic::BATTLECRY => {
                    m.battlecry = true;
                    m.needs_handler = true;
                }
                Mechanic::DEATHRATTLE => {
                    m.deathrattle = true;
                    m.needs_handler = true;
                }
                Mechanic::ENRAGED => {
                    m.enrage = true;
                    m.needs_handler = true;
                }
                other => {
                    m.unimplemented.inner.push(other.clone());
                }
            }
        }
        m
    }
}

impl<'a> From<Option<&'a Vec<Mechanic>>> for Mechanics {
    fn from(mechanics_opt: Option<&'a Vec<Mechanic>>) -> Self {
        if let Some(mechanics) = mechanics_opt {
            mechanics.into()
        } else {
            Default::default()
        }
    }
}

#[derive(
    serde_derive::Deserialize,
    serde_derive::Serialize,
    Clone,
    PartialEq,
    Debug,
    totokens_derive::ToTokens,
)]
pub enum Mechanic {
    OVERLOAD,
    TRIGGER_VISUAL,
    HEROPOWER_DAMAGE,
    SPELLPOWER,
    MODULAR,
    RUSH,
    AFFECTED_BY_SPELL_POWER,
    LIFESTEAL,
    CANT_BE_TARGETED_BY_SPELLS,
    FINISH_ATTACK_SPELL_ON_DAMAGE,
    PUZZLE,
    GRIMY_GOONS,
    KABAL,
    JADE_LOTUS,
    SUMMONED,
    ENCHANTMENT_INVISIBLE,
    GHOSTLY,
    ECHO,
    COLLECTIONMANAGER_FILTER_MANA_EVEN,
    START_OF_GAME,
    COLLECTIONMANAGER_FILTER_MANA_ODD,
    DUNGEON_PASSIVE_BUFF,
    DEATH_KNIGHT,
    CANT_BE_SILENCED,
    CANT_BE_DESTROYED,
    CANT_BE_FATIGUED,
    MULTIPLY_BUFF_VALUE,
    IGNORE_HIDE_STATS_FOR_BIG_CARD,
    SPARE_PART,
    AUTOATTACK,
    ADJACENT_BUFF,
    AI_MUST_PLAY,
    APPEAR_FUNCTIONALLY_DEAD,
    ADAPT,
    AURA,
    BATTLECRY,
    CANT_ATTACK,
    CANT_BE_TARGETED_BY_ABILITIES,
    CANT_BE_TARGETED_BY_HERO_POWERS,
    CHARGE,
    CHOOSE_ONE,
    COMBO,
    COUNTER,
    DEATHRATTLE,
    DISCOVER,
    DIVINE_SHIELD,
    ENRAGED,
    EVIL_GLOW,
    FORGETFUL,
    FREEZE,
    IMMUNE,
    INSPIRE,
    JADE_GOLEM,
    MORPH,
    POISONOUS,
    QUEST,
    RECEIVES_DOUBLE_SPELLDAMAGE_BONUS,
    RITUAL,
    SECRET,
    SILENCE,
    STEALTH,
    TAG_ONE_TURN_EFFECT,
    TAUNT,
    TOPDECK,
    UNTOUCHABLE,
    WINDFURY,
    ImmuneToSpellpower,
    InvisibleDeathrattle,
}

#[derive(
    serde_derive::Deserialize,
    serde_derive::Serialize,
    Clone,
    PartialEq,
    Debug,
    totokens_derive::ToTokens,
)]
pub enum Rarity {
    NONE,
    COMMON,
    FREE,
    RARE,
    EPIC,
    LEGENDARY,
}

#[derive(
    serde_derive::Deserialize,
    serde_derive::Serialize,
    Clone,
    PartialEq,
    Debug,
    totokens_derive::ToTokens,
)]
pub enum Set {
    TEST_TEMPORARY,
    CORE,
    EXPERT1,
    HOF,
    MISSIONS,
    DEMO,
    NONE,
    CHEAT,
    BLANK,
    DEBUG_SP,
    PROMO,
    NAXX,
    GVG,
    BRM,
    TGT,
    CREDITS,
    HERO_SKINS,
    TB,
    SLUSH,
    LOE,
    OG,
    OG_RESERVE,
    KARA,
    KARA_RESERVE,
    GANGS,
    GANGS_RESERVE,
    UNGORO,
    ICECROWN,
    LOOTAPALOOZA,
    GILNEAS,
    BOOMSDAY,
    TAVERNS_OF_TIME,
}

#[derive(
    serde_derive::Deserialize,
    serde_derive::Serialize,
    Clone,
    PartialEq,
    Debug,
    totokens_derive::ToTokens,
)]
pub enum CardClass {
    DEATHKNIGHT,
    DRUID,
    HUNTER,
    MAGE,
    PALADIN,
    PRIEST,
    ROGUE,
    SHAMAN,
    WARLOCK,
    WARRIOR,
    DREAM,
    NEUTRAL,
    WHIZBANG,
}

#[derive(
    serde_derive::Deserialize,
    serde_derive::Serialize,
    Clone,
    PartialEq,
    Debug,
    totokens_derive::ToTokens,
)]
pub enum CardUseType {
    GAME,
    PLAYER,
    HERO,
    MINION,
    SPELL,
    ENCHANTMENT,
    WEAPON,
    ITEM,
    TOKEN,
    HERO_POWER,
}

#[derive(
    serde_derive::Deserialize,
    serde_derive::Serialize,
    Clone,
    Debug,
    PartialEq,
    Eq,
    Hash,
    totokens_derive::ToTokens,
)]
pub enum PlayReq {
    REQ_MINION_TARGET,
    REQ_FRIENDLY_TARGET,
    REQ_ENEMY_TARGET,
    REQ_DAMAGED_TARGET,
    REQ_MAX_SECRETS,
    REQ_FROZEN_TARGET,
    REQ_CHARGE_TARGET,
    REQ_TARGET_MAX_ATTACK,
    REQ_NONSELF_TARGET,
    REQ_TARGET_WITH_RACE,
    REQ_TARGET_TO_PLAY,
    REQ_NUM_MINION_SLOTS,
    REQ_WEAPON_EQUIPPED,
    REQ_ENOUGH_MANA,
    REQ_YOUR_TURN,
    REQ_NONSTEALTH_ENEMY_TARGET,
    REQ_HERO_TARGET,
    REQ_SECRET_ZONE_CAP,
    REQ_MINION_CAP_IF_TARGET_AVAILABLE,
    REQ_MINION_CAP,
    REQ_TARGET_ATTACKED_THIS_TURN,
    REQ_TARGET_IF_AVAILABLE,
    REQ_MINIMUM_ENEMY_MINIONS,
    REQ_TARGET_FOR_COMBO,
    REQ_NOT_EXHAUSTED_ACTIVATE,
    REQ_UNIQUE_SECRET_OR_QUEST,
    REQ_TARGET_TAUNTER,
    REQ_CAN_BE_ATTACKED,
    REQ_ACTION_PWR_IS_MASTER_PWR,
    REQ_TARGET_MAGNET,
    REQ_ATTACK_GREATER_THAN_0,
    REQ_ATTACKER_NOT_FROZEN,
    REQ_HERO_OR_MINION_TARGET,
    REQ_CAN_BE_TARGETED_BY_SPELLS,
    REQ_SUBCARD_IS_PLAYABLE,
    REQ_TARGET_FOR_NO_COMBO,
    REQ_NOT_MINION_JUST_PLAYED,
    REQ_NOT_EXHAUSTED_HERO_POWER,
    REQ_CAN_BE_TARGETED_BY_OPPONENTS,
    REQ_ATTACKER_CAN_ATTACK,
    REQ_TARGET_MIN_ATTACK,
    REQ_CAN_BE_TARGETED_BY_HERO_POWERS,
    REQ_ENEMY_TARGET_NOT_IMMUNE,
    REQ_ENTIRE_ENTOURAGE_NOT_IN_PLAY,
    REQ_MINIMUM_TOTAL_MINIONS,
    REQ_MUST_TARGET_TAUNTER,
    REQ_UNDAMAGED_TARGET,
    REQ_CAN_BE_TARGETED_BY_BATTLECRIES,
    REQ_STEADY_SHOT,
    REQ_MINION_OR_ENEMY_HERO,
    REQ_TARGET_IF_AVAILABLE_AND_DRAGON_IN_HAND,
    REQ_LEGENDARY_TARGET,
    REQ_FRIENDLY_MINION_DIED_THIS_TURN,
    REQ_FRIENDLY_MINION_DIED_THIS_GAME,
    REQ_ENEMY_WEAPON_EQUIPPED,
    REQ_TARGET_IF_AVAILABLE_AND_MINIMUM_FRIENDLY_MINIONS,
    REQ_TARGET_WITH_BATTLECRY,
    REQ_TARGET_WITH_DEATHRATTLE,
    REQ_TARGET_IF_AVAILABLE_AND_MINIMUM_FRIENDLY_SECRETS,
    REQ_SECRET_ZONE_CAP_FOR_NON_SECRET,
    REQ_TARGET_EXACT_COST,
    REQ_STEALTHED_TARGET,
    REQ_MINION_SLOT_OR_MANA_CRYSTAL_SLOT,
    REQ_MAX_QUESTS,
    REQ_TARGET_IF_AVAILABE_AND_ELEMENTAL_PLAYED_LAST_TURN,
    REQ_TARGET_NOT_VAMPIRE,
    REQ_TARGET_NOT_DAMAGEABLE_ONLY_BY_WEAPONS,
    REQ_NOT_DISABLED_HERO_POWER,
    REQ_MUST_PLAY_OTHER_CARD_FIRST,
    REQ_HAND_NOT_FULL,
    REQ_TARGET_IF_AVAILABLE_AND_NO_3_COST_CARD_IN_DECK,
    REQ_CAN_BE_TARGETED_BY_COMBOS,
    REQ_CANNOT_PLAY_THIS,
    REQ_FRIENDLY_MINIONS_OF_RACE_DIED_THIS_GAME,
    REQ_DRAG_TO_PLAY,
    REQ_OPPONENT_PLAYED_CARDS_THIS_GAME,
}

#[derive(
    serde_derive::Deserialize,
    serde_derive::Serialize,
    Clone,
    PartialEq,
    Debug,
    totokens_derive::ToTokens,
)]
pub enum Race {
    BLOODELF,
    DRAENEI,
    DWARF,
    GNOME,
    GOBLIN,
    HUMAN,
    NIGHTELF,
    ORC,
    TAUREN,
    TROLL,
    UNDEAD,
    WORGEN,
    GOBLIN2,
    MURLOC,
    DEMON,
    SCOURGE,
    MECHANICAL,
    ELEMENTAL,
    OGRE,
    BEAST,
    TOTEM,
    NERUBIAN,
    PIRATE,
    DRAGON,
    BLANK,
    ALL,
    EGG,
}
