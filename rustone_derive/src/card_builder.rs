use proc_macro2::{Ident, TokenStream};
use quote::*;
use rustone_common::{InlineVec, Mechanics, PlayReq, Rarity};
use rustone_json::JsonCard;

pub fn build_card(card: &JsonCard, name_ident: &Ident) -> TokenStream {
    let id = card
        .id
        .expect("Expected to find a card id, but it was not present.");
    let name = card
        .name
        .as_ref()
        .expect("Expected to find a name property, but it was not present.")
        .clone();
    let rust_name = name_ident.to_string();
    let not_token_text = card.text.as_ref().cloned();
    let text = if let Some(data) = not_token_text {
        quote! { Some(String::from(#data)) }
    } else {
        quote! { None }
    };
    let cost = opt_to_tokens(card.cost);
    let attack = opt_to_tokens(card.attack);
    let health = opt_to_tokens(card.health);
    let mechanics: Mechanics = card.mechanics.as_ref().into();
    let rarity = card.rarity.as_ref().unwrap_or(&Rarity::NONE);
    let set = card
        .set
        .as_ref()
        .expect("Expected to find a set property, but it was not present.");
    let card_class = card
        .card_class
        .as_ref()
        .expect("Expected to find a card_class property, but it was not present.");
    let card_use_type = card
        .card_use_type
        .as_ref()
        .expect("Expected to find a card_use_type propery, but it was not present.");
    let play_reqs: InlineVec<PlayReq> = InlineVec::new(
        card.play_reqs
            .as_ref()
            .map_or_else(Vec::new, |pr| pr.keys().cloned().collect()),
    );

    // let handler_impl = quote! { default impl Handler for #name_ident {} };
    // let handler_impl = if !mechanics.needs_handler {
    //     Some(quote! {
    //         impl Handler for #name_ident {}
    //     })
    // } else {
    //     None
    // };

    quote! {
        pub struct #name_ident {
            name: String,
            text: Option<String>,
            mechanics: rustone_common::Mechanics,
            play_reqs: rustone_common::InlineVec<rustone_common::PlayReq>,
        }

        impl CardType for #name_ident {
            fn new() -> Self {
                use rustone_common::{Properties, Mechanics, Mechanic, InlineVec, PlayReq};
                #name_ident {
                    name: String::from(#name),
                    text: #text,
                    mechanics: #mechanics,
                    play_reqs: #play_reqs,
                }
            }

            fn id() -> u32 {
                #id
            }

            fn get_id(&self) -> u32 {
                #id
            }

            fn rust_name(&'static self) -> &'static str {
                &#rust_name
            }

            fn name(&'static self) -> &'static str {
                &self.name
            }

            fn text(&'static self) -> Option<&'static str> {
                self.text.as_ref().map(|x| &**x)
            }

            fn cost(&self) -> Option<u8> {
                #cost
            }

            fn attack(&self) -> Option<u32> {
                #attack
            }

            fn health(&self) -> Option<i32> {
                #health
            }

            fn mechs(&'static self) -> &'static rustone_common::Mechanics {
                &self.mechanics
            }

            fn rarity(&self) -> rustone_common::Rarity {
                use rustone_common::Rarity;
                #rarity
            }

            fn set(&self) -> rustone_common::Set {
                use rustone_common::Set;
                #set
            }

            fn class(&self) -> rustone_common::CardClass {
                use rustone_common::CardClass;
                #card_class
            }

            fn card_use_type(&self) -> rustone_common::CardUseType {
                use rustone_common::CardUseType;
                #card_use_type
            }

            fn play_reqs(&'static self) -> &'static [rustone_common::PlayReq] {
                &self.play_reqs.inner
            }
        }
    }
}

fn opt_to_tokens<T: ToTokens>(opt: Option<T>) -> TokenStream {
    if let Some(data) = opt {
        quote! { Some(#data) }
    } else {
        quote! { None }
    }
}
