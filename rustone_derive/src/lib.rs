#![recursion_limit = "256"]

extern crate proc_macro;

mod card_builder;

use proc_macro::TokenStream;
use proc_macro2::{Ident, Span};
use quote::*;
use rustone_json::get_card_json;
use serde_json;
use syn::{Attribute, DeriveInput, Lit, Meta};

use crate::card_builder::build_card;

#[proc_macro_derive(CardTypeGroup, attributes(class, set))]
pub fn json_card_type_derive(input: TokenStream) -> TokenStream {
    let input: DeriveInput =
        syn::parse(input).expect("Error occured in parsing the input TokenStream");
    let root_name = input.ident;
    let attrs = &input.attrs;

    let class = serde_json::from_str(&format!("\"{}\"", &get_attr_str(attrs, "class")))
        .expect("The given class attribute did not match any of the expected class types.");
    let set = serde_json::from_str(&format!("\"{}\"", &get_attr_str(attrs, "set")))
        .expect("The given set attribute did not match any of the expected set types.");

    // println!("[INFO] Number of total cards is {}", get_card_json().len());
    let cards = get_card_json().iter().filter(|card| {
        card.id.is_some()
            && card.name.is_some()
            && card.cost.is_some()
            && card.card_use_type.is_some()
            && card.card_class.as_ref() == Some(&class)
            && card.set.as_ref() == Some(&set)
    });
    let card_name_idents = cards.clone().map(|card| {
        let name = card
            .name_as_alphanumeric()
            .expect("A card name was expected, but not found.");
        // println!("[INFO] Auto-implementing set {:?}, class {:?}, card {} with id {}", &set, &class, &name, card.id.unwrap());
        Ident::new(&name, Span::call_site())
    });
    // println!("[INFO] Number of class={:?} & set={:?} cards is {}", class, set, &cards.len());

    let cloned_card_name_idents = card_name_idents.clone();
    let mut token_acc = quote! {
        use crate::card_type::{CardType, Handler, CardTypeGroup};
        use crate::registry::CardTypeRegistry;

        impl CardTypeGroup for #root_name {
            fn register(ctr: &mut CardTypeRegistry) -> &mut CardTypeRegistry {
                #( ctr.register_card::<#cloned_card_name_idents>(); )*
                ctr
            }
        }
    };

    cards.zip(card_name_idents).for_each(|(card, name_ident)| {
        token_acc.extend(build_card(card, &name_ident));
    });

    token_acc.into()
}

fn get_attr_str(attrs: &[Attribute], attr_name: &str) -> String {
    attrs
        .iter()
        .find_map(|attr| {
            if let Some(Meta::NameValue(meta_name_value)) = attr.interpret_meta() {
                if meta_name_value.ident == attr_name {
                    if let Lit::Str(id_lit) = meta_name_value.lit {
                        return Some(id_lit.value());
                    } else {
                        panic!(format!(
                            "Attribute `{}` for derive(rustone_derive::CardTypeGroup) must have a String value.",
                            attr_name
                        ));
                    }
                }
            }
            None
        })
        .unwrap_or_else(|| {
            panic!(
                "Attribute `{}` must be specified for derive(rustone_derive::CardTypeGroup).",
                attr_name
            )
        })
}
