// pub extern crate serde_json;

use lazy_static::lazy_static;
use regex::Regex;
use rustone_common::*;
use std::collections::HashMap;
use std::fs::File;

static JSON_CONTENTS: &'static str = include_str!("../cards.json");

lazy_static! {
    pub static ref CARD_JSON: Vec<JsonCard> = {
        if let Ok(file) = File::open(".cards.bin") {
            eprintln!("[INFO] Loading cached cards");
            if let Ok(contents) = bincode::deserialize_from(file) {
                return contents;
            } else {
                eprintln!("[WARN] An error occured attempting to use bincode to deseralize the contents of the .cards.bin file. Defaulting to json.")
            }
        }

        eprintln!("[INFO] Parsing cards json file");
        let parsed: Vec<JsonCard> = serde_json::from_str(JSON_CONTENTS).expect("An error occured attempting to use serde-json to deserialize the contents of the cards.json file.");
        let parsed = parsed
            .into_iter()
            .filter(|card| card.id.is_some() && card.id != Some(1912))
            .collect();
        bincode::serialize_into(
            File::create(".cards.bin").expect("An error occured creating the .cards.bin file."),
            &parsed,
        )
        .expect(
            "An error occured attempting to use bincode to serialize into the .cards.bin file.",
        );
        parsed
    };
}

#[derive(serde_derive::Deserialize, serde_derive::Serialize, Debug)]
pub struct JsonCard {
    #[serde(rename = "id")]
    pub str_id: Option<String>,

    #[serde(rename = "dbfId")]
    pub id: Option<u32>,

    pub name: Option<String>,
    pub text: Option<String>,
    pub flavor: Option<String>,
    pub artist: Option<String>,

    #[serde(rename = "cardClass")]
    pub card_class: Option<CardClass>,

    pub cost: Option<u8>,
    pub attack: Option<u32>,
    pub health: Option<i32>,

    pub mechanics: Option<Vec<Mechanic>>,
    pub rarity: Option<Rarity>,
    pub set: Option<Set>,

    #[serde(rename = "type")]
    pub card_use_type: Option<CardUseType>,

    #[serde(rename = "playRequirements")]
    pub play_reqs: Option<HashMap<PlayReq, u32>>,
    // collectible: bool,
    // elite: bool,
    // faction: String,
}

impl JsonCard {
    pub fn name_as_alphanumeric(&self) -> Option<String> {
        self.name.as_ref().map(|name| {
            let re = Regex::new("[^ A-Za-z0-9]+")
                .expect("An error occured initializing an internal regex string.");
            let no_strange_symbols = re.replace_all(name, "");
            let mut chars: Vec<char> = no_strange_symbols.chars().collect();

            let re = Regex::new(" [a-z]")
                .expect("An error occured initializing an internal regex string.");
            for m in re.find_iter(&no_strange_symbols) {
                chars[m.start() + 1] = chars[m.start() + 1].to_uppercase().nth(0).unwrap();
            }

            chars.into_iter().collect::<String>().replace(" ", "")
        })
    }
}

pub fn get_card_json() -> &'static [JsonCard] {
    &CARD_JSON
}
