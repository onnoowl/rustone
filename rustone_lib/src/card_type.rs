pub use rustone_common::*;

use crate::registry::{self, CardTypeRegistry};
use crate::state::GameState;
use std::fmt;

pub trait CardType: Handler + Sync + 'static {
    fn new() -> Self
    where
        Self: Sized;

    fn id() -> u32
    where
        Self: Sized;
    fn get_id(&self) -> u32;

    fn get() -> &'static dyn CardType
    where
        Self: Sized,
    {
        registry::get(Self::id())
    }

    fn rust_name(&'static self) -> &'static str;
    fn name(&'static self) -> &'static str;
    fn text(&'static self) -> Option<&'static str>;
    fn cost(&self) -> Option<u8>;
    fn attack(&self) -> Option<u32>;
    fn health(&self) -> Option<i32>;
    fn mechs(&'static self) -> &'static Mechanics;
    fn rarity(&self) -> Rarity;
    fn set(&self) -> Set;
    fn class(&self) -> CardClass;
    fn card_use_type(&self) -> CardUseType;
    fn play_reqs(&'static self) -> &'static [PlayReq];
}

pub trait Handler {
    fn on_battlecry(&self, _state: &mut GameState, _type_id: u32) {}
}

impl<T> Handler for T {
    default fn on_battlecry(&self, _state: &mut GameState, _type_id: u32) {
        unimplemented!();
    }
}

pub trait CardTypeGroup {
    fn register(ctr: &mut CardTypeRegistry) -> &mut CardTypeRegistry;
}

impl fmt::Debug for dyn CardType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let s = registry::get(self.get_id()); // Get a static instance of self, so we can borrow statically.
        write!(f, "CardType {{ name: {}, id: {}, text: {:?}, cost: {:?}, attack: {:?}, health: {:?}, mechs: {:?}, rarity: {:?}, set: {:?}, class: {:?}, card_use_type: {:?}, play_reqs: {:?}}}", s.name(), s.get_id(), s.text(), s.cost(), s.attack(), s.health(), s.mechs(), s.rarity(), s.set(), s.class(), s.card_use_type(), s.play_reqs())
    }
}
