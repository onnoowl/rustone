use crate::state::GameState;

#[derive(rustone_derive::CardTypeGroup)]
#[class = "NEUTRAL"]
#[set = "EXPERT1"]
pub struct Neutral;

impl Handler for Deathwing {
    fn on_battlecry(&self, _state: &mut GameState, _type_id: u32) {
        unimplemented!();
    }
}
