use crate::input::{Selection, SelectionType};
use crate::state::Player;
use quick_error::quick_error;

quick_error! {
    #[derive(Debug, Clone, PartialEq, serde_derive::Serialize, serde_derive::Deserialize)]
    pub enum GameError {
        PlayError(desc: String) {
            display("{}", &desc)
        }
        MinionNotFoundError(player: Player, index: usize, minion_array_length: usize) {
            display("Error finding player {:?}'s minion at index {}. Minion array length is {}", &player, &index, &minion_array_length)
        }
        CardNotFoundError(player: Player, index: usize, hand_array_length: usize) {
            display("Error finding player {:?}'s card at index {}. Hand array length is {}", &player, &index, &hand_array_length)
        }
        InsertIndexInvalid(player: Player, index: usize, minion_array_length: usize) {
            display("Index error inserting player {:?}'s minion at index {}. Minion array length is {}", &player, &index, &minion_array_length)
        }
        SelectionError(player: Player, selection: Selection, selection_type: SelectionType) {
            display("Error with selection type. Selection {:?} from Player {:?}'s perspective is not of type {:?}", &selection, &player, &selection_type)
        }
        HeroHasNoWeapon {
            display("The hero cannot attack because it has no weapon")
        }
        CharacterHasNoAttack {
            display("This character cannot attack (again) this turn")
        }
        MustAttackTaunt {
            display("You cannot attack this character since there is a minion with taunt")
        }
        CannotAttackStealth {
            display("You cannot attack a minion with stealth")
        }
        CardNotPlayableAsMinion {
            display("This card did not have health and attack properties, and cannot be played as a minion")
        }
        BoardFull {
            display("You cannot play this minion since the board is full")
        }
        InsertIndexNeeded {
            display("An insert index for playing this minion was not provided")
        }
        NotEnoughMana {
            display("Not enough mana")
        }
        DeckHasInvalidCard(card: u32) {
            display("The provided deck has a card with id {}, which is either invalid or unimplemented", card)
        }
        DeckInvalidLen(len: usize) {
            display("The provided deck has a length of {} instead of length 30", len)
        }
        Other(desc: String) {
            display("{}", &desc)
        }
    }
}
