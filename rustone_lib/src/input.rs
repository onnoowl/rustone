use crate::error::GameError;
use crate::state::{GameState, Player};

#[derive(Debug, Clone, Copy, PartialEq, serde_derive::Serialize, serde_derive::Deserialize)]
pub enum KeepOrDiscard {
    Keep,
    Discard,
}

#[derive(Debug, Clone, PartialEq, serde_derive::Serialize, serde_derive::Deserialize)]
pub struct CardChoices {
    pub going_first: [KeepOrDiscard; 3],
    pub going_second: [KeepOrDiscard; 4],
}

type Callback = Box<dyn Fn(&mut GameState, &Selection) -> InputNeededWithCallback>;

pub enum InputNeededWithCallback {
    Any,
    SelectWithCallback(SelectionType, Callback),
}

impl InputNeededWithCallback {
    pub fn strip_callback(&self) -> InputNeeded {
        match self {
            InputNeededWithCallback::Any => InputNeeded::Any,
            InputNeededWithCallback::SelectWithCallback(ref selection_type, _) => {
                InputNeeded::Select(selection_type.clone())
            }
        }
    }
}

impl PartialEq for InputNeededWithCallback {
    fn eq(&self, other: &InputNeededWithCallback) -> bool {
        match (self, other) {
            (InputNeededWithCallback::Any, InputNeededWithCallback::Any) => true,
            (
                InputNeededWithCallback::SelectWithCallback(selection_type1, _),
                InputNeededWithCallback::SelectWithCallback(selection_type2, _),
            ) => selection_type1 == selection_type2,
            (_, _) => false,
        }
    }
}

#[derive(Debug, Clone, PartialEq, serde_derive::Serialize, serde_derive::Deserialize)]
pub enum InputNeeded {
    Any,
    Select(SelectionType),
}

#[derive(Debug, Clone, PartialEq, serde_derive::Serialize, serde_derive::Deserialize)]
pub enum SelectionType {
    Minion,
    FriendlyMinion,
    EnemyMinion,

    Character,
    FriendlyCharacter,
    EnemyCharacter,
}

#[derive(Debug, Clone, PartialEq, serde_derive::Serialize, serde_derive::Deserialize)]
pub enum Input {
    PlayCard {
        card: usize,
        location: Option<usize>,
    },
    Attack {
        attacker: Selection,
        victim: Selection,
    },
    Select(Selection),
    EndTurn,
}

#[derive(Debug, Clone, PartialEq, serde_derive::Serialize, serde_derive::Deserialize)]
pub enum Selection {
    Hero(Player),
    Minion(Player, usize),
}

impl Selection {
    pub fn validate(
        &self,
        selection_type: &SelectionType,
        player_perspective: &Player,
    ) -> Result<(), GameError> {
        if self.is_selection_type(selection_type, player_perspective) {
            Ok(())
        } else {
            Err(GameError::SelectionError(
                player_perspective.clone(),
                self.clone(),
                selection_type.clone(),
            ))
        }
    }

    pub fn is_selection_type(
        &self,
        selection_type: &SelectionType,
        player_perspective: &Player,
    ) -> bool {
        use self::Selection::*;
        let player = player_perspective;
        match selection_type {
            SelectionType::Minion => match self {
                Minion(_, _) => true,
                _ => false,
            },
            SelectionType::FriendlyMinion => match self {
                Minion(p, _) if p == player => true,
                _ => false,
            },
            SelectionType::EnemyMinion => match self {
                Minion(p, _) if p != player => true,
                _ => false,
            },

            SelectionType::Character => true,
            SelectionType::FriendlyCharacter => match self {
                Hero(p) if p == player => true,
                Minion(p, _) if p == player => true,
                _ => false,
            },
            SelectionType::EnemyCharacter => match self {
                Hero(p) if p != player => true,
                Minion(p, _) if p != player => true,
                _ => false,
            },
        }
    }
}
