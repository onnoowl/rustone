#![feature(specialization)]
#![feature(register_attr)]

pub mod card_type;
pub mod cards;
pub mod error;
pub mod input;
pub mod manager;
pub mod message;
pub mod registry;
pub mod state;
