use super::*;
use crate::input::{Input, InputNeededWithCallback};
use crate::state::{Deck, GameState, Player};
use quick_error::quick_error;

pub struct Manager<Interface> {
    game_state: GameState,
    status: Status,
    interface: Interface,
}

impl<I: Interface> Manager<I> {
    pub fn new(interface: I, p1_deck: Deck, p2_deck: Deck) -> Result<Self, ManagerNewError> {
        if let Err(err) = p1_deck.validate() {
            return Err(ManagerNewError::InvalidP1Deck(err));
        }
        if let Err(err) = p2_deck.validate() {
            return Err(ManagerNewError::InvalidP2Deck(err));
        }
        let game_state = GameState::new(p1_deck, p2_deck);
        let status = auto_status(&game_state);
        Ok(Manager {
            game_state,
            status,
            interface,
        })
    }

    pub fn from_state(interface: I, game_state: GameState) -> Self {
        let status = auto_status(&game_state);
        Manager {
            game_state,
            status,
            interface,
        }
    }

    pub fn get_state(&self) -> &GameState {
        &self.game_state
    }

    pub fn step_turn(&mut self) {
        let opt_status = match self.status {
            Status::WaitingForCardSelection => self.perform_card_selection(),
            Status::WaitingForPlayer(ref player, ref input_needed) => {
                Self::perform_get_player_input(
                    &mut self.game_state,
                    &mut self.interface,
                    player,
                    input_needed,
                )
            }
            Status::Done(_) => None,
        };

        // opt_status is set to something if the result of our actions needs a specific status to be set,
        // otherwise, auto determine the status.
        self.status = opt_status.unwrap_or_else(|| auto_status(&self.game_state));

        if let Status::Done(ref outcome) = self.status {
            self.interface.notify_outcome(outcome);
        }
    }

    fn perform_card_selection(&mut self) -> Option<Status> {
        let choices_recieved = {
            let (first, second) = match self.game_state.whos_turn {
                Player::P1 => (&mut self.game_state.p1_state, &mut self.game_state.p2_state),
                Player::P2 => (&mut self.game_state.p2_state, &mut self.game_state.p1_state),
            };
            let first_opts = first.deck.take(3);
            let second_opts = second.deck.take(4);
            let card_choices_opt = self.interface.get_init_card_choices(
                &first_opts[0..3],
                &second_opts[0..4],
                &self.game_state.whos_turn,
            );
            match card_choices_opt {
                Some(card_choices) => {
                    first.pick_cards(&first_opts, &card_choices.going_first);
                    second.pick_cards(&second_opts, &card_choices.going_second);
                    true
                }
                None => false,
            }
        };
        if choices_recieved {
            self.game_state.inc_turn_and_mana();
            None
        } else {
            Some(Status::Done(Outcome::Draw))
        }
    }

    fn perform_get_player_input(
        game_state: &mut GameState,
        interface: &mut dyn Interface,
        player: &Player,
        input_needed: &InputNeededWithCallback,
    ) -> Option<Status> {
        let input_gotten_opt =
            interface.get_input(&*game_state, player, &input_needed.strip_callback()); // is none if the interface fails to get the user input (e.g. user disconnects)

        match input_gotten_opt {
            Some(input_gotten) => match (input_needed, input_gotten) {
                (_, Input::EndTurn) => {
                    game_state.next_turn();
                    None
                }
                (_, Input::PlayCard { card, location }) => {
                    let next_input_needed_result = game_state
                        .get_player_state_mut(player)
                        .inner
                        .play_card(card, location);
                    match next_input_needed_result {
                        Ok(next_input_needed) => {
                            Some(Status::WaitingForPlayer(player.clone(), next_input_needed))
                        }
                        Err(err) => {
                            interface.notify_error(player, &err);
                            None
                        }
                    }
                }
                (_, Input::Attack { attacker, victim }) => {
                    let result = game_state.attack(player, &attacker, &victim);
                    if let Err(error) = result {
                        interface.notify_error(player, &error);
                    }
                    None
                }
                (
                    InputNeededWithCallback::SelectWithCallback(_, callback),
                    Input::Select(ref selection),
                ) => {
                    let next_input_needed = (callback)(game_state, selection);
                    Some(Status::WaitingForPlayer(player.clone(), next_input_needed))
                }
                (InputNeededWithCallback::Any, Input::Select(_)) => None,
            },
            None => Some(Status::Done(Outcome::Won(player.other()))),
        }
    }

    pub fn start_loop(&mut self) -> &Outcome {
        loop {
            if let Status::Done(ref outcome) = self.status {
                break outcome;
            }
            self.step_turn();
        }
    }
}

fn auto_status(state: &GameState) -> Status {
    let p1_health = state.p1_state.inner.public.health;
    let p2_health = state.p2_state.inner.public.health;
    if state.turn_num == 0u32 {
        Status::WaitingForCardSelection
    } else if p1_health <= 0 && p2_health <= 0 {
        Status::Done(Outcome::Draw)
    } else if p1_health <= 0 {
        Status::Done(Outcome::Won(Player::P2))
    } else if p2_health <= 0 {
        Status::Done(Outcome::Won(Player::P1))
    } else {
        match state.whos_turn {
            Player::P1 => Status::WaitingForPlayer(Player::P1, InputNeededWithCallback::Any),
            Player::P2 => Status::WaitingForPlayer(Player::P2, InputNeededWithCallback::Any),
        }
    }
}

quick_error! {
    #[derive(Debug)]
    pub enum ManagerNewError {
        InvalidP1Deck(cause: GameError) {
            description("Player 1's deck was invalid.")
            display("Player 1's deck was invalid. Cause: {}", cause)
            cause(cause)
        }
        InvalidP2Deck(cause: GameError) {
            description("Player 2's deck was invalid.")
            display("Player 2's deck was invalid. Cause: {}", cause)
            cause(cause)
        }
    }
}
