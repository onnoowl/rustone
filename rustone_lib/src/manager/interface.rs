pub use crate::error::GameError;
pub use crate::input::{CardChoices, Input, InputNeeded};
use crate::manager::Outcome;
pub use crate::state::{GameState, Player};

pub trait Interface {
    fn get_init_card_choices(
        &mut self,
        going_first_cards: &[u32],
        going_second_cards: &[u32],
        whos_first: &Player,
    ) -> Option<CardChoices>;
    fn get_input(
        &mut self,
        game_state: &GameState,
        player: &Player,
        needed: &InputNeeded,
    ) -> Option<Input>;
    fn notify_error(&mut self, player: &Player, err: &GameError) {
        eprintln!("Player {:?} had error {}", player, err);
    }
    fn notify_new_game_state(&mut self, _game_state: &GameState) {}
    fn notify_outcome(&mut self, outcome: &Outcome) {
        match outcome {
            Outcome::Won(player) => println!("Player {:?} has won", player),
            Outcome::Draw => println!("The game has ended in a draw"),
        }
    }
}
