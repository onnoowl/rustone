use crate::input::InputNeededWithCallback;
use crate::state::Player;

pub enum Status {
    WaitingForCardSelection,
    WaitingForPlayer(Player, InputNeededWithCallback),
    Done(Outcome),
}

#[derive(Clone, Debug, PartialEq, serde_derive::Serialize, serde_derive::Deserialize)]
pub enum Outcome {
    Won(Player),
    Draw,
}
