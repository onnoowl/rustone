pub use crate::error::GameError;
pub use crate::input::{Input, InputNeeded, KeepOrDiscard};
pub use crate::manager::Outcome;
pub use crate::state::{ClientGameState, Deck, Player};

#[derive(Debug, Clone, PartialEq, serde_derive::Serialize, serde_derive::Deserialize)]
pub enum ServerMsg {
    ReplyNeeded(ReplyNeededMsg),
    NoReply(NoReplyMsg),
}

#[derive(Debug, Clone, PartialEq, serde_derive::Serialize, serde_derive::Deserialize)]
pub enum ReplyNeededMsg {
    GetDeckChoice,
    GetInitCardChoices(Vec<u32>),
    GetInput(Box<ClientGameState>, InputNeeded),
}

#[derive(Debug, Clone, PartialEq, serde_derive::Serialize, serde_derive::Deserialize)]
pub enum NoReplyMsg {
    Init(Player),
    NotifyError(GameError),
    NotifyNewGameState(Box<ClientGameState>),
    NotifyOutcome(Outcome),
}

#[derive(Debug, Clone, PartialEq, serde_derive::Serialize, serde_derive::Deserialize)]
pub enum ClientMsg {
    GiveDeckChoice(Deck),
    GiveInitCardChoices(Vec<KeepOrDiscard>),
    GiveInput(Input),
}
