use crate::card_type::{CardType, CardTypeGroup};
use crate::cards::{core, expert};
use lazy_static::lazy_static;
use std::collections::HashMap;

#[derive(Default)]
pub struct CardTypeRegistry {
    registry_map: HashMap<u32, Box<dyn CardType>>,
}

impl CardTypeRegistry {
    pub fn register<CTG: CardTypeGroup>(&mut self) -> &mut Self {
        CTG::register(self)
    }

    pub fn register_card<CT: CardType>(&mut self) -> &mut Self {
        let ct = Box::new(CT::new());
        self.registry_map.insert(CT::id(), ct);
        self
    }

    fn get_card_type(&'static self, id: u32) -> Option<&'static dyn CardType> {
        self.registry_map.get(&id).map(|val| &**val)
        //The &** converts the &Box<dyn CardType> to just a &dyn CardType
    }
}

pub fn get(id: u32) -> &'static dyn CardType {
    CARD_TYPE_REGISTER.get_card_type(id).unwrap_or_else(|| {
        panic!(
            "Error, card {} was not registered, but was requested from the registry",
            id
        )
    })
}

pub fn try_get(id: u32) -> Option<&'static dyn CardType> {
    CARD_TYPE_REGISTER.get_card_type(id)
}

pub fn iter() -> impl Iterator<Item = &'static (dyn CardType + 'static)> {
    CARD_TYPE_REGISTER.registry_map.values().map(|v| &**v)
}

lazy_static! {
    static ref CARD_TYPE_REGISTER: CardTypeRegistry = {
        let mut ctr = CardTypeRegistry::default();
        ctr.register::<core::Neutral>()
            .register::<expert::Neutral>();
        ctr
    };
}
