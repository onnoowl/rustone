use crate::error::GameError;

pub trait Attackable {
    fn get_attack(&self) -> Result<u32, GameError>;
    fn damage(&mut self, amt: u32);
    fn heal(&mut self, amt: u32);
    fn dec_attacks_remaining(&mut self);
}
