// use super::*;
use crate::registry;

#[derive(Clone, Debug, PartialEq, serde_derive::Serialize, serde_derive::Deserialize)]
pub struct CardInHand {
    pub type_id: u32,
    pub cost: u8,
}

impl CardInHand {
    pub fn new(type_id: u32) -> Self {
        CardInHand {
            type_id,
            cost: registry::get(type_id).cost().unwrap_or(0),
        }
    }
}
