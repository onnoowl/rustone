use rand::{thread_rng, Rng};

use super::*;
use crate::error::GameError;
use crate::registry;

// A `Deck` is a collection of 30 cards you would make, pick from, have sorted, and stored.
#[derive(Default, Clone, Debug, PartialEq, serde_derive::Serialize, serde_derive::Deserialize)]
pub struct Deck {
    pub name: String,
    pub cards: ArrayVec<[u32; 30]>,
}

impl Deck {
    pub fn new(name: String) -> Self {
        Deck {
            name,
            cards: Default::default(),
        }
    }

    pub fn is_complete(&self) -> bool {
        self.cards.is_full()
    }

    pub fn len(&self) -> usize {
        self.cards.len()
    }

    pub fn is_empty(&self) -> bool {
        self.cards.is_empty()
    }

    pub fn is_valid(&self) -> bool {
        self.validate().is_ok()
    }

    pub fn validate(&self) -> Result<(), GameError> {
        if !self.is_complete() {
            return Err(GameError::DeckInvalidLen(self.cards.len()));
        }
        for card in &self.cards {
            if registry::try_get(*card).is_none() {
                return Err(GameError::DeckHasInvalidCard(*card));
            }
        }
        Ok(())
    }
}

// A `GameDeck`, or In-Game Deck, is one made from a `Deck` that is shuffled, and can have additional cards put into it during the game.
#[derive(Clone, Debug, PartialEq, serde_derive::Serialize, serde_derive::Deserialize)]
pub struct GameDeck {
    pub cards: Vec<u32>,
}

impl GameDeck {
    pub fn draw(&mut self) -> Option<u32> {
        self.cards.pop()
    }

    // Panics if not enough cards. (Intended for use at start of game with all 30 cards)
    pub fn take(&mut self, count: usize) -> Vec<u32> {
        let mut v = Vec::with_capacity(count);
        for _ in 0..count {
            v.push(self.cards.pop().unwrap());
        }
        v
    }

    pub fn append(&mut self, vec: &mut Vec<u32>) {
        self.cards.append(vec);
    }

    pub fn shuffle(&mut self) {
        thread_rng().shuffle(&mut self.cards);
    }
}

impl From<Deck> for GameDeck {
    fn from(deck: Deck) -> Self {
        let mut game_deck = GameDeck {
            cards: deck.cards.into_iter().collect(),
        };
        game_deck.shuffle();
        game_deck
    }
}
