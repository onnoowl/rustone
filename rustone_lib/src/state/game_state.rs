use super::*;
use crate::error::GameError;
use crate::input::{Selection, SelectionType};

#[derive(Clone, Debug, PartialEq, serde_derive::Serialize, serde_derive::Deserialize)]
pub struct GameState {
    pub p1_state: ServerPlayerState,
    pub p2_state: ServerPlayerState,
    pub turn_num: u32,
    pub whos_turn: Player,
}

impl GameState {
    pub fn new(p1_deck: Deck, p2_deck: Deck) -> Self {
        GameState {
            p1_state: ServerPlayerState::new(Player::P1, p1_deck),
            p2_state: ServerPlayerState::new(Player::P2, p2_deck),
            turn_num: 0u32,
            whos_turn: rand::random(),
        }
    }

    pub fn get_for_client(&self, player: &Player) -> ClientGameState {
        let (me, op) = match player {
            Player::P1 => (&self.p1_state, &self.p2_state),
            Player::P2 => (&self.p2_state, &self.p1_state),
        };

        let whos_turn = if &self.whos_turn == player {
            WhosTurn::MyTurn
        } else {
            WhosTurn::OpTurn
        };

        ClientGameState {
            my_state: me.inner.clone(),
            op_state: op.inner.public.clone(),
            turn_num: self.turn_num,
            whos_turn,
        }
    }

    pub fn get_player_state(&self, player: &Player) -> &ServerPlayerState {
        match player {
            Player::P1 => &self.p1_state,
            Player::P2 => &self.p2_state,
        }
    }

    pub fn get_player_state_mut(&mut self, player: &Player) -> &mut ServerPlayerState {
        match player {
            Player::P1 => &mut self.p1_state,
            Player::P2 => &mut self.p2_state,
        }
    }

    pub fn attack(
        &mut self,
        player: &Player,
        attacker: &Selection,
        victim: &Selection,
    ) -> Result<(), GameError> {
        attacker.validate(&SelectionType::FriendlyCharacter, player)?;
        victim.validate(&SelectionType::EnemyCharacter, player)?;

        let (me, op) = match player {
            Player::P1 => (&mut self.p1_state, &mut self.p2_state),
            Player::P2 => (&mut self.p2_state, &mut self.p1_state),
        };
        me.inner.attack(attacker, victim, &mut op.inner.public)
    }

    pub fn next_turn(&mut self) {
        self.whos_turn.swap();
        self.inc_turn_and_mana();
    }

    pub fn inc_turn_and_mana(&mut self) {
        self.turn_num += 1_u32;
        match self.whos_turn {
            Player::P1 => self.p1_state.next_turn(),
            Player::P2 => self.p2_state.next_turn(),
        }
    }

    pub fn flip_perspective(&mut self) {
        std::mem::swap(&mut self.p1_state, &mut self.p2_state);
        self.whos_turn.swap();
    }
}

#[derive(Clone, Debug, PartialEq, serde_derive::Serialize, serde_derive::Deserialize)]
pub struct ClientGameState {
    pub my_state: PlayerState,
    pub op_state: PublicPlayerState,
    pub turn_num: u32,
    pub whos_turn: WhosTurn,
}

#[derive(Clone, Debug, PartialEq, serde_derive::Serialize, serde_derive::Deserialize)]
pub enum WhosTurn {
    MyTurn,
    OpTurn,
}
