use super::*;
use crate::error::GameError;
use crate::registry;

use rustone_common::Properties;

#[derive(Clone, Debug, PartialEq, serde_derive::Serialize, serde_derive::Deserialize)]
pub struct MinionOnBoard {
    pub type_id: u32,
    pub health: i32,
    pub attack: u32,
    pub attacks_remaining: u32,
    pub props: Properties,
}

impl MinionOnBoard {
    pub fn new(type_id: u32) -> Result<Self, GameError> {
        let type_instance = registry::get(type_id);
        let health = type_instance
            .health()
            .ok_or(GameError::CardNotPlayableAsMinion)? as i32;
        let attack = type_instance
            .attack()
            .ok_or(GameError::CardNotPlayableAsMinion)?;
        let props = type_instance.mechs().props.clone();
        let attacks_remaining = match (props.charge, props.windfury) {
            (true, true) => 2,
            (true, false) => 1,
            (false, _) => 0,
        };

        Ok(MinionOnBoard {
            type_id,
            health,
            attack,
            attacks_remaining,
            props,
        })
    }
}

impl Attackable for MinionOnBoard {
    fn get_attack(&self) -> Result<u32, GameError> {
        if self.attacks_remaining == 0u32 {
            return Err(GameError::CharacterHasNoAttack);
        }
        Ok(self.attack)
    }
    fn damage(&mut self, amt: u32) {
        if self.props.divine_shield {
            self.props.divine_shield = false;
        } else {
            self.health -= amt as i32;
        }
    }
    fn heal(&mut self, amt: u32) {
        let max_health = registry::get(self.type_id).health().unwrap_or_else(|| {
            panic!(
                "Card type id {} didn't have a health ammount when it should have.",
                self.type_id
            )
        }) as i32;
        self.health += amt as i32;
        if self.health > max_health {
            self.health = max_health;
        }
    }
    fn dec_attacks_remaining(&mut self) {
        self.attacks_remaining -= 1u32;
        if self.props.stealth {
            self.props.stealth = false;
        }
    }
}
