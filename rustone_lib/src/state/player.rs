use rand::{
    distributions::{Distribution, Standard},
    Rng,
};

#[derive(Clone, Debug, PartialEq, serde_derive::Serialize, serde_derive::Deserialize)]
pub enum Player {
    P1,
    P2,
}

impl Player {
    pub fn other(&self) -> Self {
        match self {
            Player::P1 => Player::P2,
            Player::P2 => Player::P1,
        }
    }
    pub fn swap(&mut self) {
        *self = self.other();
    }
}

impl Distribution<Player> for Standard {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> Player {
        match rng.gen_range(0, 2) {
            0 => Player::P1,
            _ => Player::P2,
        }
    }
}
