use super::*;
use crate::error::GameError;
use crate::input::Selection;
use crate::input::{InputNeededWithCallback, KeepOrDiscard};
use crate::registry;

use rustone_common::CardUseType;

#[derive(Clone, Debug, PartialEq, serde_derive::Serialize, serde_derive::Deserialize)]
pub struct ServerPlayerState {
    // Server Level Knowledge
    pub deck: GameDeck,
    pub inner: PlayerState,
}

#[derive(Clone, Debug, PartialEq, serde_derive::Serialize, serde_derive::Deserialize)]
pub struct PlayerState {
    // Client Level Knowledge that you see as a player
    pub hand: ArrayVec<[CardInHand; 10]>,
    pub public: PublicPlayerState,
}

#[derive(Clone, Debug, PartialEq, serde_derive::Serialize, serde_derive::Deserialize)]
pub struct PublicPlayerState {
    // Client Level Knowledge that you and other players can see
    pub player: Player,
    pub board: ArrayVec<[MinionOnBoard; 7]>,
    pub health: i32,
    pub mana: u8,
    pub weapon: Option<Weapon>,
    pub attacks_remaining: u32,
}

impl ServerPlayerState {
    pub fn new(player: Player, deck: Deck) -> Self {
        ServerPlayerState {
            deck: deck.into(),
            inner: PlayerState {
                hand: Default::default(),
                public: PublicPlayerState {
                    player,
                    board: Default::default(),
                    health: 30,
                    mana: 0,
                    weapon: None,
                    attacks_remaining: 0,
                },
            },
        }
    }

    pub fn draw_card(&mut self) {
        if let Some(card) = self.deck.draw() {
            if !self.inner.hand.is_full() {
                self.inner.hand.push(CardInHand::new(card));
            }
        } else {
            self.inner.public.health -= 2;
        }
    }

    pub fn next_turn(&mut self) {
        self.draw_card();
        self.inner.public.inc_mana();
        self.inner.public.reset_attacks();
    }

    pub fn pick_cards(&mut self, opts: &[u32], choices: &[KeepOrDiscard]) {
        let (mut keeps, mut discards) = ServerPlayerState::sort_keeps_and_discards(opts, choices);
        let discarded_count = opts.len() - keeps.len();
        if discarded_count > 0 {
            keeps.append(&mut self.deck.take(discarded_count));
        }
        self.deck.append(&mut discards);
        self.deck.shuffle();
    }

    fn sort_keeps_and_discards(options: &[u32], choices: &[KeepOrDiscard]) -> (Vec<u32>, Vec<u32>) {
        let (keep, discard): (Vec<_>, Vec<_>) =
            options
                .iter()
                .zip(choices.iter())
                .partition(|(_card, keep_or_discard)| match keep_or_discard {
                    KeepOrDiscard::Keep => true,
                    KeepOrDiscard::Discard => false,
                });
        (
            keep.iter().map(|(card, _)| **card).collect(),
            discard.iter().map(|(card, _)| **card).collect(),
        )
    }
}

impl PlayerState {
    pub fn play_card(
        &mut self,
        card_index: usize,
        location_index: Option<usize>,
    ) -> Result<InputNeededWithCallback, GameError> {
        let card = self.get_card(card_index)?.clone();
        let type_instance = registry::get(card.type_id);

        match type_instance.card_use_type() {
            CardUseType::MINION => {
                // Make sure we have the index to insert minion into the board at
                let index = location_index.ok_or(GameError::InsertIndexNeeded)?;

                // Error checking
                if self.public.board.is_full() {
                    return Err(GameError::BoardFull);
                }
                if index >= self.public.board.len() {
                    return Err(GameError::InsertIndexInvalid(
                        self.public.player.clone(),
                        index,
                        self.public.board.len(),
                    ));
                }
                if self.public.mana < card.cost {
                    return Err(GameError::NotEnoughMana);
                }

                // Use mana and insert
                self.public.mana -= card.cost;
                self.public
                    .board
                    .insert(index, MinionOnBoard::new(card.type_id)?);

                Ok(InputNeededWithCallback::Any)
            }
            _ => Err(GameError::Other(
                "Only minion cards have been implemented so far.".to_string(),
            )), //TODO: add non-minions
        }
    }

    pub fn get_card(&self, index: usize) -> Result<&CardInHand, GameError> {
        self.hand.get(index).ok_or_else(|| {
            GameError::CardNotFoundError(self.public.player.clone(), index, self.public.board.len())
        })
    }

    pub fn attack(
        &mut self,
        attacker: &Selection,
        victim: &Selection,
        opponent: &mut PublicPlayerState,
    ) -> Result<(), GameError> {
        use self::Selection::*;

        if self.public.board_has_taunt() {
            return Err(GameError::MustAttackTaunt);
        }

        {
            let att: &mut dyn Attackable = match attacker {
                Hero(_) => &mut self.public,
                Minion(_, index) => self.public.get_minion_mut(*index)?,
            };

            let vic: &mut dyn Attackable = match victim {
                Hero(_) => opponent,
                Minion(_, index) => {
                    let minion = opponent.get_minion_mut(*index)?;
                    if minion.props.stealth {
                        return Err(GameError::CannotAttackStealth);
                    }
                    minion
                }
            };

            att.damage(vic.get_attack()?);
            att.dec_attacks_remaining();
            vic.damage(att.get_attack()?);
        }

        self.public.clear_dead_minions();
        opponent.clear_dead_minions();

        Ok(())
    }
}

impl PublicPlayerState {
    pub fn inc_mana(&mut self) {
        if self.mana < 10_u8 {
            self.mana += 1u8;
        }
    }

    pub fn reset_attacks(&mut self) {
        self.attacks_remaining = 1;
        self.board.iter_mut().for_each(|minion| {
            if minion.props.windfury {
                minion.attacks_remaining = 2;
            } else {
                minion.attacks_remaining = 1;
            }
        });
    }

    pub fn get_minion(&self, index: usize) -> Result<&MinionOnBoard, GameError> {
        self.board.get(index).ok_or_else(|| {
            GameError::MinionNotFoundError(self.player.clone(), index, self.board.len())
        })
    }

    pub fn get_minion_mut(&mut self, index: usize) -> Result<&mut MinionOnBoard, GameError> {
        let potential_err =
            GameError::MinionNotFoundError(self.player.clone(), index, self.board.len());
        self.board.get_mut(index).ok_or(potential_err)
    }

    pub fn board_has_taunt(&self) -> bool {
        self.board.iter().any(|minion| minion.props.taunt)
    }

    pub fn clear_dead_minions(&mut self) {
        self.board.retain(|minion| minion.health > 0);
    }
}

impl Attackable for PublicPlayerState {
    fn get_attack(&self) -> Result<u32, GameError> {
        if self.attacks_remaining == 0u32 {
            return Err(GameError::CharacterHasNoAttack);
        }
        self.weapon
            .as_ref()
            .ok_or(GameError::HeroHasNoWeapon)
            .map(|weapon| weapon.attack)
    }
    fn damage(&mut self, amt: u32) {
        self.health -= amt as i32;
    }
    fn heal(&mut self, amt: u32) {
        self.health += amt as i32;
        if self.health > 30 {
            self.health = 30;
        }
    }
    fn dec_attacks_remaining(&mut self) {
        self.attacks_remaining -= 1u32;
        let should_destroy_weapon = {
            let mut wep = self
                .weapon
                .as_mut()
                .expect("Weapon not found. This character should not have been allowed to attack.");
            wep.durability -= 1u32;
            wep.durability == 0u32
        };
        if should_destroy_weapon {
            self.weapon = None;
        }
    }
}
