#[derive(Clone, Debug, PartialEq, serde_derive::Serialize, serde_derive::Deserialize)]
pub struct Weapon {
    pub type_id: u32,
    pub attack: u32,
    pub durability: u32,
}

impl Weapon {
    pub fn new(type_id: u32, attack: u32, durability: u32) -> Self {
        Weapon {
            type_id,
            attack,
            durability,
        }
    }
}
