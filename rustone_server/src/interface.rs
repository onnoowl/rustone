use bincode;
use rustone_lib::manager::{
    self, CardChoices, GameError, GameState, Input, InputNeeded, Outcome, Player,
};
use rustone_lib::message::{ClientMsg, Deck, KeepOrDiscard, NoReplyMsg, ReplyNeededMsg, ServerMsg};
use std::net::TcpStream;

use crate::msg_stream::MsgStream;

pub struct Interface {
    p1_stream: MsgStream,
    p2_stream: MsgStream,
}

fn get_deck_choice(stream: &mut MsgStream) -> bincode::Result<Deck> {
    stream.write(&ServerMsg::ReplyNeeded(ReplyNeededMsg::GetDeckChoice));
    match stream.read_result() {
        Ok(ClientMsg::GiveDeckChoice(deck)) => Ok(deck),
        Ok(other) => {
            Err(Box::new(bincode::ErrorKind::Custom(format!("Error, client did not respond with `GiveDeckChoice(deck)` message type when prompted for it. Responded with: {:?}", other))))
        }
        Err(err) => {
            Err(Box::new(bincode::ErrorKind::Custom(format!("Error getting deck choices from clients: {}", err))))
        }
    }
}

fn get_init_card_choice(stream: &mut MsgStream, cards: &[u32]) -> Option<Vec<KeepOrDiscard>> {
    stream.write(&ServerMsg::ReplyNeeded(ReplyNeededMsg::GetInitCardChoices(
        cards.to_vec(),
    )))?;
    match stream.read()? {
        ClientMsg::GiveInitCardChoices(choices) => Some(choices),
        other => {
            eprintln!("Error, client did not respond with `GiveInitCardChoices(choices)` message type when prompted for it. Responded with: {:?}", other);
            None
        }
    }
}

impl Interface {
    pub fn new(p1_tcp_stream: TcpStream, p2_tcp_stream: TcpStream) -> bincode::Result<Self> {
        let mut p1_stream = MsgStream::new(p1_tcp_stream);
        let mut p2_stream = MsgStream::new(p2_tcp_stream);
        p1_stream.write_result(&ServerMsg::NoReply(NoReplyMsg::Init(Player::P1)))?;
        p2_stream.write_result(&ServerMsg::NoReply(NoReplyMsg::Init(Player::P2)))?;
        Ok(Interface {
            p1_stream,
            p2_stream,
        })
    }

    pub fn get_deck_choices(&mut self) -> bincode::Result<(Deck, Deck)> {
        Ok((
            get_deck_choice(&mut self.p1_stream)?,
            get_deck_choice(&mut self.p2_stream)?,
        ))
    }

    fn get_stream(&mut self, player: &Player) -> &mut MsgStream {
        match player {
            Player::P1 => &mut self.p1_stream,
            Player::P2 => &mut self.p2_stream,
        }
    }
}

impl manager::Interface for Interface {
    fn get_init_card_choices(
        &mut self,
        going_first_cards: &[u32],
        going_second_cards: &[u32],
        whos_first: &Player,
    ) -> Option<CardChoices> {
        let (going_first_stream, going_second_stream) = match whos_first {
            Player::P1 => (&mut self.p1_stream, &mut self.p2_stream),
            Player::P2 => (&mut self.p2_stream, &mut self.p1_stream),
        };
        let first_choice_vec = get_init_card_choice(going_first_stream, going_first_cards)?;
        let second_choice_vec = get_init_card_choice(going_second_stream, going_second_cards)?;
        if first_choice_vec.len() != 3 {
            eprintln!("Error, player {:?} responded with a choice vector of length {} instead of length 3", whos_first, first_choice_vec.len());
            return None;
        }
        if first_choice_vec.len() != 4 {
            eprintln!("Error, player {:?} responded with a choice vector of length {} instead of length 4", whos_first, first_choice_vec.len());
            return None;
        }
        let mut first_choice = [KeepOrDiscard::Keep; 3];
        let mut second_choice = [KeepOrDiscard::Keep; 4];
        first_choice.copy_from_slice(&first_choice_vec);
        second_choice.copy_from_slice(&second_choice_vec);
        Some(CardChoices {
            going_first: first_choice,
            going_second: second_choice,
        })
    }

    fn get_input(
        &mut self,
        game_state: &GameState,
        player: &Player,
        needed: &InputNeeded,
    ) -> Option<Input> {
        let stream = self.get_stream(player);
        stream.write(&ServerMsg::ReplyNeeded(ReplyNeededMsg::GetInput(
            Box::new(game_state.get_for_client(player)),
            needed.clone(),
        )))?;
        match stream.read()? {
            ClientMsg::GiveInput(input) => Some(input),
            other => {
                eprintln!("Error, client did not respond with `GiveInput(input)` message type when prompted for input. Responded with: {:?}", other);
                None
            }
        }
    }

    fn notify_error(&mut self, player: &Player, err: &GameError) {
        let msg = ServerMsg::NoReply(NoReplyMsg::NotifyError(err.clone()));
        self.get_stream(player).write(&msg);
    }

    fn notify_new_game_state(&mut self, game_state: &GameState) {
        let msg1 = ServerMsg::NoReply(NoReplyMsg::NotifyNewGameState(Box::new(
            game_state.get_for_client(&Player::P1),
        )));
        let msg2 = ServerMsg::NoReply(NoReplyMsg::NotifyNewGameState(Box::new(
            game_state.get_for_client(&Player::P2),
        )));
        self.p1_stream.write(&msg1);
        self.p2_stream.write(&msg2);
    }

    fn notify_outcome(&mut self, outcome: &Outcome) {
        let msg = ServerMsg::NoReply(NoReplyMsg::NotifyOutcome(outcome.clone()));
        self.p1_stream.write(&msg);
        self.p2_stream.write(&msg);
    }
}
