mod interface;
mod msg_stream;

use crate::interface::Interface;

use std::net::{TcpListener, TcpStream};
use std::thread;

use rustone_lib::manager::Manager;

fn run_game(p1_stream: TcpStream, p2_stream: TcpStream) -> bincode::Result<()> {
    let mut interface = Interface::new(p1_stream, p2_stream)?;
    let (p1_deck, p2_deck) = interface.get_deck_choices()?;
    let mut manager = Manager::new(interface, p1_deck, p2_deck).map_err(|err| {
        Box::new(bincode::ErrorKind::Custom(format!(
            "Error intializing manager: {}",
            err
        )))
    })?;
    manager.start_loop();
    Ok(())
}

fn main() {
    let addr = "0.0.0.0:25565";
    let listener = TcpListener::bind(addr)
        .unwrap_or_else(|err| panic!("Failed to bind to addr {} with error: {}", addr, err));

    let mut last_stream_opt: Option<TcpStream> = None;

    // accept connections and process them serially
    for stream_result in listener.incoming() {
        match stream_result {
            Ok(stream) => {
                if let Some(last_stream) = last_stream_opt.take() {
                    thread::spawn(move || {
                        if let Err(err) = run_game(last_stream, stream) {
                            eprintln!("Error occured setting up a game: {}", err); //TODO: notify clients of graceful cancellation
                        }
                    });
                } else {
                    last_stream_opt = Some(stream);
                }
            }
            Err(err) => eprintln!("Error opening stream to client: {}", err),
        }
    }
}
