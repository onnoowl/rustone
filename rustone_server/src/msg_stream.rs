use bincode::{self, deserialize_from, serialize_into};
use std::net::TcpStream;

use rustone_lib::message::{ClientMsg, ServerMsg};

pub struct MsgStream {
    inner: TcpStream,
}

impl MsgStream {
    pub fn new(stream: TcpStream) -> Self {
        MsgStream { inner: stream }
    }

    pub fn read(&mut self) -> Option<ClientMsg> {
        self.read_result()
            .map_err(|err| {
                eprintln!(
                    "Server encountered the following error while reading: {}",
                    err
                )
            })
            .ok()
    }

    pub fn write(&mut self, msg: &ServerMsg) -> Option<()> {
        self.write_result(msg)
            .map_err(|err| {
                eprintln!(
                    "Server encountered the following error while writing: {}",
                    err
                )
            })
            .ok()
    }

    pub fn read_result(&mut self) -> bincode::Result<ClientMsg> {
        deserialize_from(&mut self.inner)
    }

    pub fn write_result(&mut self, msg: &ServerMsg) -> bincode::Result<()> {
        serialize_into(&mut self.inner, msg)
    }
}
