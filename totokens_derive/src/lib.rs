extern crate proc_macro;

use proc_macro::TokenStream;
use proc_macro2::{Punct, Spacing, Span};
use syn::{Data, DeriveInput, LitStr};

use quote::*;

#[proc_macro_derive(ToTokens)]
pub fn to_tokens_derive(input: TokenStream) -> TokenStream {
    let input: DeriveInput =
        syn::parse(input).expect("Error occured in parsing the input TokenStream");
    let name = input.ident;
    match input.data {
        Data::Struct(data) => {
            let props_let = data.fields.iter().cloned().map(|field| {
                let ident = field.ident.expect(
                    "Error, derive(ToTokens) is not implemented for Structs with unnamed fields.",
                );
                quote! {
                    let #ident = self.#ident.clone();
                }
            });
            let props_set = data.fields.iter().cloned().map(|field| {
                let ident = field.ident.expect(
                    "Error, derive(ToTokens) is not implemented for Structs with unnamed fields.",
                );
                let hashtag = Punct::new('#', Spacing::Alone);
                quote! {
                    #ident: #hashtag #ident
                }
            });
            let generated = quote! {
                impl quote::ToTokens for #name {
                    fn to_tokens(&self, tokens: &mut proc_macro2::TokenStream) {
                        use quote::*;
                        use proc_macro2::{Spacing, Span, Punct, Ident};
                        #(#props_let)*
                        let code = quote! {
                            #name {
                                #(#props_set),*
                            }
                        };
                        tokens.extend(code);
                    }
                }
            };
            generated.into()
        }
        Data::Enum(data) => {
            let name_lit = LitStr::new(&name.to_string(), Span::call_site());
            let match_branches = data.variants.into_iter().map(|var| {
                if var.fields.iter().next().is_some() {
                    panic!(
                        "Error, derive(ToTokens) is not implemented for Enums with data fields."
                    );
                }
                let var_str = LitStr::new(&var.ident.to_string(), Span::call_site());
                quote! {
                    #name::#var => tokens.append(Ident::new(#var_str, Span::call_site()))
                }
            });
            let generated = quote! {
                impl quote::ToTokens for #name {
                    fn to_tokens(&self, tokens: &mut proc_macro2::TokenStream) {
                        use quote::TokenStreamExt;
                        use proc_macro2::{Spacing, Span, Punct, Ident};

                        tokens.append(Ident::new(#name_lit, Span::call_site()));
                        tokens.append(Punct::new(':', Spacing::Joint));
                        tokens.append(Punct::new(':', Spacing::Alone));
                        match *self {
                            #(#match_branches),*
                        }
                    }
                }
            };
            generated.into()
        }
        Data::Union(_) => panic!("Error, derive(ToTokens) is not implemented for Unions."),
    }
}
